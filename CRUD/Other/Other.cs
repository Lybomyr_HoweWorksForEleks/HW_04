﻿namespace CRUD.Other
{
    public static class Other
    {
        public static int GetIndex(string[] objects, string o)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (o == objects[i])
                    return i;
            }
            return -1;
        }
    }
}
