﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CRUD.Objects
{
    [Serializable]
    public class Person : IComparable<Person>
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = "unknown";
        public string LastName { get; set; } = "unknown";

        public override string ToString()
        {
            return $"Name: {FirstName}\nSurname: {LastName}";
        }

        public int CompareTo(Person other)
        {
            if (Id > other.Id)
                return 1;
            if (Id < other.Id)
                return -1;
            return 0;
        }
    }
}
