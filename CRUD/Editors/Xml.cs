﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using CRUD.Objects;

namespace CRUD.Editors
{
    static class Xml
    {
        static XmlSerializer serializer = new XmlSerializer(typeof(List<Person>));

        public static void SerializeXml(List<Person> persons, string path)
        {
            //Цей рядок був потрібний, інакше XMl файл не очиститься
            File.Delete(path);
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                serializer.Serialize(fs, persons);
            }
        }

        public static List<Person> DeSerialize(string path)
        {
            List<Person> users;

            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                users = (List<Person>) serializer.Deserialize(fs);
            }
            return users;
        }
    }
}
