﻿using System.Collections.Generic;
using CRUD.Crud;
using CRUD.Objects;
using CRUD.Patterns.Adapter;

namespace CRUD.Editors
{
    /// <summary>
    /// Файловий редактор
    /// </summary>
    public class FileEditor
    {
        private string _path;
        public bool Flag { get; set; }

        public FileEditor()
        {
            _path = "";
            Flag = true;
        }

        private Application.Storage _storage;
        private IStorage _storageEditor;
        public FileEditor(Application.Storage storage)
        {
            _storage = storage;
        }
        
        /// <summary>
        /// Запис сховища у файл
        /// </summary>
        /// <param name="data"></param>
        public void Write(Storage data)
        {
            try
            {
                _storageEditor.Save();
            }
            catch
            {
                Flag = false;
            }
        }
        /// <summary>
        /// Читання списку
        /// </summary>
        /// <returns></returns>
        public List<Person> Read()
        {
            try
            {
                return _storageEditor.Read();
            }
            catch
            {
                Flag = false;
                return null;
            }
        }
        /// <summary>
        /// Просто визначення шляху.
        /// </summary>
        /// <param name="name"></param>
        public void OpenOrCreate(string name)
        {
            _path = name;
            Flag = true;
            switch (_storage)
            {
                case Application.Storage.Json:
                    _storageEditor=new JsonStorage(_path);
                    break;
                case Application.Storage.Xml:
                    _storageEditor=new XmlStorage(_path);
                    break;
            }
        }
    }
}
