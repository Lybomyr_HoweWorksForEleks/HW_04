﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD.Crud;
using static CRUD.Crud.Application;

namespace CRUD.Patterns.Interpreter
{
    public class SetXmlStorage : SetStorage
    {
        public override Tuple<Storage, string> Interpret()
        {
            Storage storageType = Storage.Xml;
            string format = ".xml";
            return new Tuple<Storage, string>(storageType, format);
        }
    }
}
