﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using CRUD.Objects;

namespace CRUD.Patterns.Adapter
{
    class DatabaseStorage: IStorage
    {
        private const string TableName = "Person";
        private const string FirstNameColumn = "FirstName";
        private const string LastNameColumn = "LastName";
        private const string IdColumn = "Id";

        //Add connection string
        private readonly string _connectionString = "";

        private List<Person> _people;
        public List<Person> People
        {
            get
            {
                if (_people == null)
                {
                    Read();
                }
                return _people;
            }
            protected set { _people = value; }
        }

        public DatabaseStorage()
        {
            People = new List<Person>();
        }

        public void Add(Person person)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string queryString = $"INSERT INTO {TableName}" +
                                     $"({FirstNameColumn}, {LastNameColumn}) " +
                                     $"VALUES ({person.FirstName}, {person.LastName})";
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                    People.Add(person);
                }
            }
        }

        public Person Get(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string queryString = $"SELECT * FROM {TableName}" +
                                     $"WHERE {IdColumn}={id}";
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        connection.Close();
                        Person person = new Person
                        {
                            Id = id,
                            FirstName = reader[FirstNameColumn].ToString(),
                            LastName = reader[LastNameColumn].ToString()
                        };
                        return person;
                    }
                }
            }
        }

        public List<Person> Read()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string queryString = $"SELECT * FROM {TableName}";
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Person person = new Person
                            {
                                Id = int.Parse(reader[IdColumn].ToString()),
                                FirstName = reader[FirstNameColumn].ToString(),
                                LastName = reader[LastNameColumn].ToString()
                            };
                            People.Add(person);
                        }
                    }
                    connection.Close();
                }
            }
            return People;
        }

        public void Update(Person person, int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string queryString = $"UPDATE {TableName} " +
                                     $"SET {FirstNameColumn}='{person.FirstName}', " +
                                     $"{LastNameColumn}='{person.LastName}' " +
                                     $"WHERE {IdColumn}={id}";
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    int index = People.FindIndex(p => p.Id == id);
                    People[index] = person;
                }
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string queryString = $"DELETE FROM {TableName} " +
                                     $"WHERE {IdColumn}={id}";
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    var person = People.FirstOrDefault(p => p.Id == id);
                    People.Remove(person);
                }
            }
        }

        public void Save()
        {
            throw new System.NotImplementedException();
        }
    }
}
