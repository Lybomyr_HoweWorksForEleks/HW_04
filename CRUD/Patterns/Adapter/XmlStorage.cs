﻿using System.Collections.Generic;
using CRUD.Editors;
using CRUD.Objects;

namespace CRUD.Patterns.Adapter
{
    class XmlStorage: FileStorage
    {
        public XmlStorage(string filename) : base(filename)
        {
        }

        public override List<Person> Read()
        {
            People = Xml.DeSerialize(Filename);
            return People;
        }

        public override void Save()
        {
            Xml.SerializeXml(People, Filename);
        }
    }
}
