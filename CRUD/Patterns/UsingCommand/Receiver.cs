﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD.Crud;
using CRUD.Objects;
using static CRUD.Other.Other;

namespace CRUD.Patterns.UsingCommand
{
    class Receiver
    {
        public Storage _storage { get; set; }

        public void Add(string[] arguments)
        {
            Person person = new Person();
            if (arguments.Contains("-firstname"))
            {
                var index = GetIndex(arguments, "-firstname");
                if (index + 1 >= 0 && index + 1 < arguments.Length)
                {
                    person.FirstName = arguments[index + 1];
                }
            }
            if (arguments.Contains("-lastname"))
            {
                var index = GetIndex(arguments, "-lastname");
                if (index + 1 >= 0 && index + 1 < arguments.Length)
                {
                    person.LastName = arguments[index + 1];
                }
            }
            _storage.Add(person);
        }

        public void Update(string[] arguments)
        {
            var index = GetIndex(arguments, "-userId");
            if (index + 1 >= 0)
            {
                if (index + 1 >= 0 && index + 1 < arguments.Length)
                {
                    var person = _storage.Persons.First(x => x.Id == int.Parse(arguments[index + 1]));

                    if (arguments.Contains("-firstname"))
                    {
                        index = GetIndex(arguments, "-firstname");
                        if (index + 1 >= 0 && index + 1 < arguments.Length)
                        {
                            person.FirstName = arguments[index + 1];
                        }
                    }
                    if (arguments.Contains("-lastname"))
                    {
                        index = GetIndex(arguments, "-lastname");
                        if (index + 1 >= 0 && index + 1 < arguments.Length)
                        {
                            person.LastName = arguments[index + 1];
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Person isn`t founded in storage");
            }
        }

        public void Get(string[] arguments)
        {
            var index = GetIndex(arguments, "-userId");
            if (index >= 0)
            {
                var person = _storage.Persons.FirstOrDefault(x => x.Id == int.Parse(arguments[index + 1].Trim()));
                Console.WriteLine(person.ToString());
            }
            else
            {
                Console.WriteLine("Person isn`t founded in storage");
            }
        }

        public void Delete(string[] arguments)
        {
            var index = GetIndex(arguments, "-userId");
            if (index + 1 >= 0)
            {
                _storage.Delete(int.Parse(arguments[index + 1]));
            }
            else
            {
                Console.WriteLine("Person isn`t founded in storage");
            }
        }
    }
}
