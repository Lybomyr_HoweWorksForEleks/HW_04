﻿using CRUD.Patterns.UsingCommand;

namespace CRUD.Patterns.Chain
{
	class UpdateHandler : IChain
	{
		public bool Process(string command)
		{
			if (command == "update")
			{
				Client.UpdateCommand();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
