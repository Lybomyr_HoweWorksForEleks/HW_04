﻿using CRUD.Patterns.UsingCommand;

namespace CRUD.Patterns.Chain
{
	class DeleteHandler : IChain
	{
		public bool Process(string command)
		{
			if (command == "delete")
			{
				Client.DeleteCommand();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
