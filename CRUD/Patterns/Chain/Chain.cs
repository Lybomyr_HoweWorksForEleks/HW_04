﻿using System.Collections.Generic;

namespace CRUD.Patterns.Chain
{
	public class Chain
	{
		private List<IChain> list;

		public List<IChain> List
		{
			get
			{
				if (list == null)
				{
					list = new List<IChain>();
				}

				return list;
			}
		}

		public Chain()
		{
            list=new List<IChain>();
		}

		public void Message(string command)
		{
			foreach (IChain item in list)
			{
				bool result = item.Process(command);

				if (result)
					break;
			}
		}

		public void Add(IChain handler)
		{
			this.list.Add(handler);
		}
	}

}
