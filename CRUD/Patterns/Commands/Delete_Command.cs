﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Patterns.UsingCommand
{
    class Delete_Command : Command
    {
        public Delete_Command(Receiver receiver)
            :base(receiver)
        {

        }

        public override void Execute()
        {
            receiver.Delete(arguments);
        }
    }
}
