﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Patterns.UsingCommand
{
    class Create_Command : Command
    {
        public Create_Command(Receiver receiver)
            :base(receiver)
        {

        }

        public override void Execute()
        {
            receiver.Add(arguments);
        }
    }
}
